# -*- coding: utf-8 -*-

""" Service Stream Keep Playing  (c)  2016 tuxfrank

# This program is free software; you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation;
# either version 2 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program;
# if not, see <http://www.gnu.org/licenses/>.


"""
import json
import xbmc
import xbmcgui
import xbmcaddon
import re
 
addonid_ = 'service.stream-keepplaying'
addon_       = xbmcaddon.Addon(addonid_)
addonname_   = addon_.getAddonInfo('name')
icon_=addon_.getAddonInfo('icon')
waittime_ = 15           #[seconds]
notificationtime_ = 5000  #[milliseconds]
videocountdowncycles_ = 4       #[-]
audiocountdowncycles_ = 1       #[-]



if __name__ == '__main__':
    videocountdown_ = 0
    audiocountdown_ = 0
    monitor_ = xbmc.Monitor()
    while not monitor_.abortRequested():
        shownotifications_ = addon_.getSetting(id='ShowNotifications')
        # Sleep/wait for abort for 15 seconds
        if monitor_.waitForAbort(waittime_):
            # Abort was requested while waiting. We should exit
            break

        timeremaining_= xbmc.getInfoLabel('Player.TimeRemaining');
        isstreambefore_= timeremaining_=='';  
        mediatype_= xbmc.getInfoLabel('VideoPlayer.Title')
        playingisvideo_= mediatype_ != ""
        # Get the url of the media that is playing, if it ends with a slash, remove that.
        # If url ends with a /, it is not started by the player because it is considered a directory.
        isplaying_ = xbmc.getInfoLabel('Player.Filenameandpath');
        isplaying_ = re.sub('/$','',isplaying_);
        timeremaining_= xbmc.getInfoLabel('Player.TimeRemaining');
        isstreamafter_= timeremaining_=='';  

        if isstreambefore_ and isstreamafter_ and isplaying_[0:6]!="nfs://" and isplaying_[0:6]!="smb://":
            isstream_ = True
        else:
            isstream_ = False
            
        #xbmc.log('=========================================================',xbmc.LOGNOTICE)
        #xbmc.log('mediatype:\n\'%s\''%mediatype_,xbmc.LOGNOTICE)                          
        #xbmc.log('temp:\n\'%s\''%xbmc.getInfoLabel('VideoPlayer.Title'),xbmc.LOGNOTICE)    
        #xbmc.log('playingisvideo:\n\'%s\''%playingisvideo_,xbmc.LOGNOTICE)                 
        #xbmc.log('isplaying:\n\'%s\''%isplaying_,xbmc.LOGNOTICE)                                                            
        #xbmc.log('timeremaining:\n\'%s\''%timeremaining_,xbmc.LOGNOTICE)                                                    
        #xbmc.log('isstreambefore:\n\'%s\''%isstreambefore_,xbmc.LOGNOTICE)                                                  
        #xbmc.log('isstreamafter:\n\'%s\''%isstreamafter_,xbmc.LOGNOTICE)                                                    

        if playingisvideo_:   
            videocountdown_= videocountdowncycles_    # A video file is currently playing
        else:
            if videocountdown_!=0:
                videocountdown_=videocountdown_-1   # no video isplaying
                if shownotifications_=="true":
                    xbmc.executebuiltin('Notification(%s,videocountdown:%i,%d,%s)' %(addonname_,videocountdown_,notificationtime_,icon_)) 
            else:
                storedstream_ = addon_.getSetting(id='CurrentStream')
                if isplaying_=='':    # Nothing is playing
                    if audiocountdown_!=0:
                        audiocountdown_=audiocountdown_-1   # no audio isplaying
                        if shownotifications_=="true":
                            xbmc.executebuiltin('Notification(%s,audiocountdown:%i,%d,%s)' %(addonname_,audiocountdown_,notificationtime_,icon_)) 
                    else:        
                        audiocountdown_=audiocountdowncycles_
                        xbmc.executebuiltin('PlayMedia(%s)'%(storedstream_))
                        if shownotifications_=="true":
                            xbmc.executebuiltin('Notification(%s,StartStream:%s,%d,%s)' %(addonname_,storedstream_,notificationtime_,icon_)) 
                else:  # Something is playing
                    # next line creates .kodi/userdata/addon_data/plugin.program.hello.world/settings.xml
                    # but getSetting does not work if there's no resources/settings.xml file in the addons
                    # original directory
                    if isplaying_ == storedstream_:   # Currently playing is not equal to previously playing
                        if shownotifications_=="true":
                            xbmc.executebuiltin('Notification(%s,isplaying:%s,%d,%s)' %(addonname_,isplaying_,notificationtime_,icon_))
                    else:
                        if isstream_:   # New stream is playing That must be stored in settings
                            audiocountdown_=audiocountdowncycles_
                            addon_.setSetting(id='CurrentStream',value=isplaying_)
                            if shownotifications_=="true":
                                xbmc.executebuiltin('Notification(%s,StoredNewStream:%s,%d,%s)' %(addonname_,isplaying_,notificationtime_,icon_))
                        else:  #currently playing is not a stream
                            audiocountdown_=videocountdowncycles_
                            if (shownotifications_=="true"):
                                xbmc.executebuiltin('Notification(%s,%s,%d,%s)' %(addonname_,'Something other than a stream is playing',notificationtime_,icon_))
        # Check if the correct audio device is still active
        forceaudiodevsettings_= addon_.getSetting(id='ForceAudioDevSettings')
        if forceaudiodevsettings_=="true": 
            storeddevice_= addon_.getSetting(id='StoredDevice')
            jsonrequest_=xbmc.executeJSONRPC('{"jsonrpc":"2.0","method":"Settings.GetSettingValue", "params":{"setting":"audiooutput.audiodevice"},"id":1}')
            jsonresponse_=json.loads(jsonrequest_)
            currdevice_=jsonresponse_['result']['value']
            #xbmc.log('=========================================================',xbmc.LOGNOTICE)
            #xbmc.log('storeddevice:\n %s'%storeddevice_,xbmc.LOGNOTICE)
            #xbmc.log('jsonrequest:\n %s'%jsonrequest_,xbmc.LOGNOTICE)
            #xbmc.log('jsonresponse:\n %s'%jsonresponse_,xbmc.LOGNOTICE)
            #xbmc.log('currdevice:\n %s'%currdevice_,xbmc.LOGNOTICE)
            #xbmc.log('check:\n %s'%(storeddevice_!=currdevice_),xbmc.LOGNOTICE)
      
            if storeddevice_!=currdevice_:
                audiodevupdate_= addon_.getSetting(id='AudioDevUpdate')
                #xbmc.log('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++',xbmc.LOGNOTICE)
                #xbmc.log('audiodevupdate:\n %s'%audiodevupdate_,xbmc.LOGNOTICE)
                if audiodevupdate_=="true":
                    #xbmc.log('---------------------------------------------------------',xbmc.LOGNOTICE)
                    #xbmc.log('command currdevice:\n %s'%currdevice_,xbmc.LOGNOTICE)
                    addon_.setSetting(id='StoredDevice',value=currdevice_)
                    addon_.setSetting(id='AudioDevUpdate',value="false")
                    xbmc.executeJSONRPC('{"jsonrpc":"2.0", "method":"Settings.SetSettingValue", "params":{"setting":"audiooutput.passthroughdevice","value":"%s"}, "id":1}' % currdevice_ )              
                else:
                    #xbmc.log('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++',xbmc.LOGNOTICE)
                    #xbmc.log('audiodevreset:\n %s'%storeddevice_,xbmc.LOGNOTICE)
                    xbmc.executeJSONRPC('{"jsonrpc":"2.0", "method":"Settings.SetSettingValue", "params":{"setting":"audiooutput.audiodevice","value":"%s"}, "id":1}' % storeddevice_ )
                    ## Sleep/wait for abort for 1 seconds
                    #if monitor_.waitForAbort(1):
                    #    # Abort was requested while waiting. We should exit
                    #    break
                    xbmc.executeJSONRPC('{"jsonrpc":"2.0", "method":"Settings.SetSettingValue", "params":{"setting":"audiooutput.passthroughdevice","value":"%s"}, "id":1}' % storeddevice_ )              
    
    
