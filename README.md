# kodi.stream-keepplaying
Kodi add-on to keep (audio/radio) streams playing

Tested with the Kodi Radio Add-on (created by Tristan Fischer).

## Functionality
- Every 15 seconds it's checked if a stream should be playing, if so, it's started.
- Every 15 seconds it's checked if the output device is still set correctly, if not, it is corrected. The correct audio device can be set through the configuration menu. If the audio output device and passthrough device are set correctly in kodi, choose the configuration option: "Update audio device to currently active device".
- If a video or audio track is started, the previously running stream is halted.
- If the video or audio track playing has ended, the previously running stream is started again after approximately one minute.
- When kodi is (re)started the previously running stream is started after approximately 15 seconds.
- When an audio stream is interrupted, kodi will keep trying to restart it.
- If a different audio stream is started, this new stream will be stored as the previously running stream.
- Output device checking can be disabled in the configuration menu.
- Notifications can be disabled in the configuration menu. Notifications are never displayed when a video is playing.
